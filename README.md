# UnoNet Ethernet board
![UnoNet Ethernet board](https://ilabs.se/wp-content/uploads/2022/03/2019-08-06T14-23-56.771Z-unonet-3-1287x965-1.jpg)

## UnoNet Ethernet board
The UnoNet is a microcontroller board based on the ATmega328 and W5500 ethernet controller. It has 14 digital input/output pins, 6 analog inputs, a 16 MHz crystal oscillator, a RJ45 connection, a power jack, an ICSP header, and a reset button. The board has been laid out as a Arduino Rev 3 compatible board and is equipped with Arduino compatible headers making it possible to fit standard Arduino compatible shields on top of it.

## UnoNet Ethernet board Key features
- ATMega328PB 8-bit microcontroller
- 32KB Flash, 2KB SRAM
- W5500 Ethernet controller
- UART, SPI, TWI
- PWM
- 12-bit ADC
- Silabs CP2104 USB to serial adapter

## The board

### Ethernet
The W5500 chip is a Hardwired TCP/IP embedded Ethernet controller that enables easier internet connection for embedded systems using an SPI (Serial Peripheral Interface) connection. W5500 will fit users in need of stable internet connectivity best, using a single chip to implement TCP/IP Stack, 10/100 Ethernet MAC and PHY. Hardwired TCP/IP stack supports TCP, UDP, IPv4, ICMP, ARP, IGMP, and PPPoE, …, which has been proven through various applications over many years. W5500 uses a 32Kbytes internal buffer as its data communication memory.

### Power
There are two onboard voltage regulators, one 5V and one 3.3V, that runs the onboard electronic devices. The board can be supplied by an external 7.5V - 12V voltage source or from a single USB connector.

### Pinout
The board follows the recommended Uno Rev3 pinout from Arduino as described here:
<img src="https://ilabs.se/wp-content/uploads/2022/03/unonet-pinout-diagram-v0.1-1024x651.png" alt="drawing"/>

### Software support
The board is supported by the Arduino environment.

## License
This design covered by the CERN Open Hardware Licence v1.2 and a copy of this license is also available in this repo.

## iLabs
<img src="https://ilabs.se/wp-content/uploads/2021/07/cropped-Color-logo-no-background.png" alt="drawing" width="200"/>

### About us
Invector Labs is a small Swedish engineering company that designs and build electronic devices for hobbyists as well as small and medium sized businesses.

For more information about this board you can visit the product page at our [website](https://ilabs.se/product/unonet-arduino-uno-board-with-ethernet-atmega328pb/)

Questions about this product can be addressed to <oshwa@ilabs.se>.

/* EOF */

